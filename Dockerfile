FROM node:8-alpine

COPY src/ ./home/node/webapps/
WORKDIR ./home/node/webapps/

RUN npm install --production 

EXPOSE 3000
CMD ["node", "server.js"]