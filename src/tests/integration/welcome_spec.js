describe('Homepage specs', () => {
  it ('Should contains hello worlds text', () => {
    cy.visit('/')
    cy.get('p').should('exist')
    cy.get('p').should('contain', 'Hello worlds!')
  })
})